package es.almata.dam.practica.empresa;

public class Run {

	public static void main(String[] args) {
		Empresa empresa = new Empresa("48052852K","Arnau","Carrer Menarguens 1 2A");
		Treballador O1 = new Operaris("Genis","48052853E",12);
		Treballador O2 = new Operaris("Arnau","48053852K",12);
		Treballador E1 = new Enginyers("Marc","48762853J",12);
		Treballador E2 = new Enginyers("Mireia","48067822A",12);
		Treballador A1 = new Administratius("Jaume","48125433X",12);
		Treballador A2 = new Administratius("Laura","12349654F",12);
		Nomina n1 = new Nomina(1, 2021, 123456789);
		Nomina n2 = new Nomina(2, 2021, 123456789);
		Nomina n3 = new Nomina(3, 2021, 123456789);
		Nomina n4 = new Nomina(4, 2021, 123456789);
		Nomina n5 = new Nomina(5, 2021, 123456789);
		Nomina n6 = new Nomina(6, 2021, 123456789);
		Nomina n7 = new Nomina(7, 2021, 123456789);
		Nomina n8 = new Nomina(8, 2021, 123456789);
		Nomina n9 = new Nomina(9, 2021, 123456789);
		Nomina n10 = new Nomina(10, 2021, 123456789);
		Nomina n11 = new Nomina(11, 2021, 123456789);
		Nomina n12 = new Nomina(12, 2021, 123456789);
		O1.addNomina(n1);
		O1.addNomina(n2);
		O1.addNomina(n3);
		O1.addNomina(n4);
		O1.addNomina(n5);
		O1.addNomina(n6);
		O1.addNomina(n7);
		O1.addNomina(n8);
		O1.addNomina(n9);
		O1.addNomina(n10);
		O1.addNomina(n11);
		O1.addNomina(n12);
		E1.addNomina(n1);
		E1.addNomina(n2);
		E1.addNomina(n3);
		E1.addNomina(n4);
		E1.addNomina(n5);
		E1.addNomina(n6);
		E1.addNomina(n7);
		E1.addNomina(n8);
		E1.addNomina(n9);
		E1.addNomina(n10);
		E1.addNomina(n11);
		E1.addNomina(n12);
		A1.addNomina(n1);
		A1.addNomina(n2);
		A1.addNomina(n3);
		A1.addNomina(n4);
		A1.addNomina(n5);
		A1.addNomina(n6);
		A1.addNomina(n7);
		A1.addNomina(n8);
		A1.addNomina(n9);
		A1.addNomina(n10);
		A1.addNomina(n11);
		A1.addNomina(n12);
		
		empresa.calcularTotal(A1, n1);
		empresa.calcularTotal(A1, n2);
		empresa.calcularTotal(A1, n3);
		empresa.calcularTotal(A1, n4);
		empresa.calcularTotal(A1, n5);
		empresa.calcularTotal(A1, n6);
		empresa.calcularTotal(A1, n7);
		empresa.calcularTotal(A1, n8);
		empresa.calcularTotal(A1, n9);
		empresa.calcularTotal(A1, n10);
		empresa.calcularTotal(A1, n11);
		empresa.calcularTotal(A1, n12);
		empresa.calcularTotal(O1, n1);
		empresa.calcularTotal(O1, n2);
		empresa.calcularTotal(O1, n3);
		empresa.calcularTotal(O1, n4);
		empresa.calcularTotal(O1, n5);
		empresa.calcularTotal(O1, n6);
		empresa.calcularTotal(O1, n7);
		empresa.calcularTotal(O1, n8);
		empresa.calcularTotal(O1, n9);
		empresa.calcularTotal(O1, n10);
		empresa.calcularTotal(O1, n11);
		empresa.calcularTotal(O1, n12);
		empresa.calcularTotal(E1, n1);
		empresa.calcularTotal(E1, n2);
		empresa.calcularTotal(E1, n3);
		empresa.calcularTotal(E1, n4);
		empresa.calcularTotal(E1, n5);
		empresa.calcularTotal(E1, n6);
		empresa.calcularTotal(E1, n7);
		empresa.calcularTotal(E1, n8);
		empresa.calcularTotal(E1, n9);
		empresa.calcularTotal(E1, n10);
		empresa.calcularTotal(E1, n11);
		empresa.calcularTotal(E1, n12);
	
		
		
		empresa.addTreballador(A1);
		empresa.addTreballador(A2);
		empresa.addTreballador(E1);
		empresa.addTreballador(E2);
		empresa.addTreballador(O1);
		empresa.addTreballador(O2);
		
		System.out.println(empresa);
	}

}
