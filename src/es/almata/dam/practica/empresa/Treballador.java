package es.almata.dam.practica.empresa;

import java.util.ArrayList;

public class Treballador {
	//Atributs
	private String nom;
	private String dni;
	private double preuHora;
	private int horesTreballades;
	private ArrayList<Nomina> nomines = new ArrayList<Nomina>();
	private Empresa empresa;
	
	//Constructor
	public Treballador() {
		super();
	}
	public Treballador(String nom, String dni, double preuHora, int horesTreballades) {
		super();
		this.nom = nom;
		this.dni = dni;
		this.preuHora = preuHora;
		this.horesTreballades = horesTreballades;
	}

	//Metodes de Negoci
	public void addNomina(Nomina nomina) {
		nomines.add(nomina);
	}
	public ArrayList<Nomina> getAllNomines(){
		return nomines;
	}
	public Nomina getNomina(int index) {
		return nomines.get(index);
	}

	//Setters & getters
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getdni() {
		return dni;
	}
	public void setdni(String dni) {
		this.dni = dni;
	}
	public double getPreuHora() {
		return preuHora;
	}
	public void setPreuHora(double preuHora) {
		this.preuHora = preuHora;
	}
	public int getHoresTreballades() {
		return horesTreballades;
	}
	public void setHoresTreballades(int horesTreballades) {
		this.horesTreballades = horesTreballades;
	}

	@Override
	public String toString() {
		return "Treballador nom = " + nom + ", dni = " + dni + ", preuHora = " + preuHora + ", horesTreballades = "
				+ horesTreballades + ", nomines = " + nomines + "\n";
	}
	
	

}
