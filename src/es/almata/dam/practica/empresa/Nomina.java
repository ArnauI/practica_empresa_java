package es.almata.dam.practica.empresa;

public class Nomina {
	//Atributs
	private int mes;
	private int any;
	private int id;
	private double totalNomina;
	private Treballador treballador;
	
	//Constructors
	public Nomina() {
	}
	public Nomina(int mes, int any, int id) {
		super();
		this.mes = mes;
		this.any = any;
		this.id = id;
	}
	//Setters & getters
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAny() {
		return any;
	}
	public void setAny(int any) {
		this.any = any;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getTotalNomina() {
		return totalNomina;
	}
	public void setTotalNomina(double totalNomina) {
		this.totalNomina = totalNomina;
	}
	@Override
	public String toString() {
		return "Nomina mes = " + mes + ", any = " + any + ", id = " + id + ", totalNomina = " + totalNomina;
	}
	
	
}
