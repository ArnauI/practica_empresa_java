package es.almata.dam.practica.empresa;

import java.util.ArrayList;

public class Empresa {
	//Atributs
	private String nif;
	private String nom;
	private String adreca;
	private ArrayList<Treballador> treballadors = new ArrayList<Treballador>();

	
	public void calcularTotal(Treballador treballador, Nomina nomina) {
		nomina.setTotalNomina(treballador.getHoresTreballades()*treballador.getPreuHora()); 
	}
	
	//Constructors
	public Empresa() {
	}
	public Empresa(String nif, String nom, String adreca) {
		super();
		this.nif = nif;
		this.nom = nom;
		this.adreca = adreca;
	}

	//Metodes de Negoci
	public void addTreballador(Treballador treballador) {
		treballadors.add(treballador);
	}
	
	public ArrayList<Treballador> getAllTreballadors(){
		return treballadors;
	}
	
	public Treballador getTreballador(int index) {
		return treballadors.get(index);
	}

	//Setters & getters
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public String getAdreca() {
		return adreca;
	}
	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}
	public void setTreballadors(ArrayList<Treballador> treballadors) {
		this.treballadors = treballadors;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Empresa nif = " + nif + ", nom = " + nom + ", adreca = " + adreca + ", treballadors = " + treballadors;
	}
	
}
